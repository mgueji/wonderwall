#!/bin/bash

my_location=""
my_user=""

curl -s -o /home/$my_user/.weather wttr.in/$my_location?format="%l"
curl -s wttr.in/$my_location?format="%C" >> /home/$my_user/.weather
curl -s wttr.in/$my_location?format="%t" >> /home/$my_user/.weather

my_file_location=$(sed -n '1p' /home/$my_user/.weather)
my_file_condition=$(sed -n '2p' /home/$my_user/.weather)
my_file_temperature=$(sed -n '3p' /home/$my_user/.weather)

if [ -f "/home/$my_user/.last_weather" ]; then
    if [ ".weather" -nt ".last_weather" ]; then
        echo "This will never be seen."
    else
        convert -font Ubuntu-Bold -pointsize 50 -fill white -stroke black -strokewidth 1 -draw 'text 60,260 "$my_file_location"' /home/$my_user/Pictures/$my_file_condition.jpg /home/$my_user/Pictures/weawall.jpg
        convert -font Ubuntu-Bold -pointsize 90 -fill white -stroke black -strokewidth 1 -draw 'text 60,200 "$my_file_temperature"' /home/$my_user/Pictures/weawall.jpg /home/$my_user/Pictures/weawall.jpg
        gsettings set org.gnome.desktop.background picture-uri file:///home/$my_user/Pictures/weawall.jpg
    fi
else
    cp /home/$my_user/{.weather,.last_weather}
fi