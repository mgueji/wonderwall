# WonderWall

Tiny script to set wallpaper according to the current weather.

Needs a cron job to download the current forecast every x time.

`*/30 * * * * /bin/bash -c "/home/$MAIN_USER/test.sh"`